#!/usr/bin/env python
import cv2
import math
import numpy as np
import pdb
import random
import scipy.spatial

def homogeneous_point(p):
    return np.float32([p[0], p[1], 1])

def cartesian_point(p):
    if p[2] != 0:
        return p[0]/p[2], p[1]/p[2]
    else:
        print("point {} is infinite".format(p))
        return None

def line(p1, p2):
    return np.cross(p1, p2)

def intersection(l1, l2):
    return np.cross(l1, l2)

def prt(pt,fpfmt=".1f"):
    fmt = "{{:{}}}".format(fpfmt)
    return ','.join([fmt.format(p) for p in pt])

def q1():
#    pdb.set_trace()
    
    f = 2000.0
    points = ((-30, -50), (120, -30), (100, 50), (-30, 60))
    h_points = [homogeneous_point(p) for p in points]

    l1 = line(h_points[0], h_points[3])
    print("$l_{{p_0, p_3}} = ({}) \\times ({}) = [{}]$".format(prt(h_points[0]), prt(h_points[3]), prt(l1)))
    l2 = line(h_points[1], h_points[2])
    print("$l_{{p_1, p_2}} = ({}) \\times ({}) = [{}]$".format(prt(h_points[1]), prt(h_points[2]), prt(l2)))

    intersection_1 = intersection(l1, l2)
    print("$v_1 = [{}] \\times [{}] = ({})$".format(prt(l1), prt(l2), prt(intersection_1)))
    
    l3 = line(h_points[0], h_points[1])
    print("$l_{{p_0, p_1}} = ({}) \\times ({}) = [{}]$".format(prt(h_points[0]), prt(h_points[1]), prt(l3)))
    l4 = line(h_points[3], h_points[2])
    print("$l_{{p_3, p_2}} = ({}) \\times ({}) = [{}]$".format(prt(h_points[3]), prt(h_points[2]), prt(l4)))

    intersection_2 = intersection(l3, l4)
    print("$v_2 = [{}] \\times [{}] = ({})$".format(prt(l3), prt(l4), prt(intersection_2)))


    horizon_line =  line(intersection_1, intersection_2)
    print("$h = ({}) \\times ({}) = [{}]$".format(prt(intersection_1), prt(intersection_2), prt(horizon_line)))

    print("vanishing points: {}, {}".format(cartesian_point(intersection_1),
                                            cartesian_point(intersection_2)))
    print("Horizon line: {}".format(horizon_line))

    cp_int_1 = cartesian_point(intersection_1)
    cp_int_2 = cartesian_point(intersection_2)

    print("$v_{{1_c}} = ({}/{}, {}/{}) = ({})$".format(
        intersection_1[0], intersection_1[2], 
        intersection_1[1], intersection_1[2], 
        prt(cartesian_point(intersection_1))))

    print("$v_{{2_c}} = ({}/{}, {}/{}) = ({})$".format(
        intersection_2[0], intersection_2[2], 
        intersection_2[1], intersection_2[2], 
        prt(cartesian_point(intersection_2))))

    orth = np.cross(np.float32([cp_int_1[0], cp_int_1[1], f]),
                    np.float32([cp_int_2[0], cp_int_2[1], f]))

    print("$N = [{}] \\times [{}] = [{}]".format(prt([cp_int_1[0], cp_int_1[1], f]),
                                                prt([cp_int_2[0], cp_int_2[1], f]),
                                                prt(orth)))

    print("Normal to plane: {}".format(orth))

    A = orth[0]
    B = orth[1]
    C = orth[2]

    points_3d = []
    for i, pt in enumerate(points):
        u, v = pt
        denom = (A * u + B * v + C * f)
        c = -f / denom
        b = -v / denom
        a = -u / denom
        denom_str = "{:.2f} \\cdot {:.2f} + {:.2f} \\cdot {:.2f} + {:.2f} \\cdot {:.2f}".format(
            A, u, B, v, C, f)
#        print("({}, {}) -> ({}D, {}D, {}D)".format(u,v, a, b, c))
        print("$a_{} = \\frac{{-Du}}{{Au + Bv + Cf}} = \\frac{{-{}D}}{{{}}} = {}D$\\\\*".format(i, u, denom_str, a))
        print("$b_{} = \\frac{{-Dv}}{{Au + Bv + Cf}} = \\frac{{-{}D}}{{{}}} = {}D$\\\\*".format(i, v, denom_str, b))
        print("$c_{} = \\frac{{-Df}}{{Au + Bv + Cf}} = \\frac{{-{}D}}{{{}}} = {}D$\\\\*".format(i, f, denom_str, c))
        points_3d.append(np.float32([a,b,c]))

    print("Distance from $\\mathbf{{p_0}}$ to $\\mathbf{{p_1}} = \\|\\mathbf{{p_0}} - \\mathbf{{p_1}}\\| = {}$\\\\*".format(scipy.spatial.distance.euclidean(points_3d[0], points_3d[1])))
    print("Distance from $\\mathbf{{p_3}}$ to $\\mathbf{{p_2}} = \\|\\mathbf{{p_3}} - \\mathbf{{p_2}}\\| = {}$\\\\*".format(scipy.spatial.distance.euclidean(points_3d[3], points_3d[2])))
    print("Distance from $\\mathbf{{p_0}}$ to $\\mathbf{{p_3}} = \\|\\mathbf{{p_0}} - \\mathbf{{p_3}}\\| = {}$\\\\*".format(scipy.spatial.distance.euclidean(points_3d[0], points_3d[3])))
    print("Distance from $\\mathbf{{p_1}}$ to $\\mathbf{{p_2}} = \\|\\mathbf{{p_1}} - \\mathbf{{p_2}}\\| = {}$\\\\*".format(scipy.spatial.distance.euclidean(points_3d[1], points_3d[2])))

    ratio = scipy.spatial.distance.euclidean(points_3d[0], points_3d[1])/scipy.spatial.distance.euclidean(points_3d[1], points_3d[2])
    print("Ratio of long side to short side = ${} / {} = {}$".format(scipy.spatial.distance.euclidean(points_3d[0], points_3d[1]),
                                                                     scipy.spatial.distance.euclidean(points_3d[1], points_3d[2]),
                                                                     ratio))
    print("Dimensions: 100m x {}m".format(ratio* 100))

    print("$\\| \\mathbf{{p_0}} - \\mathbf{{p_3}} \\| = 100m$\\\\*")
    print("$\\sqrt{{(a_0 - a_3) ^ 2 + (b_0 - b_3) ^ 2 + (c_0 - c_3) ^ 2}} = 100m$\\\\*")
    print("$(a_0 - a_3) ^ 2 + (b_0 - b_3) ^ 2 + (c_0 - c_3) ^ 2 = 10000m^2$\\\\*")
    print("$({}D - {}D) ^2 + ({}D - {}D) ^2 + ({}D - {}D)^2 = 10000m^2$\\\\*".format(
        points_3d[0][0],points_3d[3][0],
        points_3d[0][1],points_3d[3][1], 
        points_3d[0][2],points_3d[3][2]))
    d_a = points_3d[0][0] - points_3d[3][0]
    d_b = points_3d[0][1] - points_3d[3][1]
    d_c = points_3d[0][2] - points_3d[3][2]
    print("$({}D) ^2 + ({}D) ^2 + ({}D)^2 = 10000m^2$\\\\*".format(d_a, d_b, d_c))

    d_a_s = (points_3d[0][0] - points_3d[3][0]) ** 2
    d_b_s = (points_3d[0][1] - points_3d[3][1]) ** 2
    d_c_s = (points_3d[0][2] - points_3d[3][2]) ** 2
    
    print("${}D^2 + {}D^2 + {}D^2 = 10000m^2$\\\\*".format(d_a_s, d_b_s, d_c_s))

    print("$D = \\sqrt(\\frac{{10000}}{{{}}})m$\\\\*".format(d_a_s+d_b_s+d_c_s))
    D = math.sqrt(10000./d_a_s+d_b_s+d_c_s)
    print("$D = {}m$\\\\*".format(D))
    D = D * 10000.
    print("$D = {} px$\\\\*".format(D))

    # A(uc/f) + B(vc/f) + Cc + D = 0
    # D = -(A(uc/f) + B(vc/f) + Cc)
    A = orth[0]
    B = orth[1]
    C = orth[2]
    print("$D =  {}".format(D))
    print("Equation of plane: ${}x + {}y {}z + {} = 0$".format(A,B,C,D))
    for u,v in points:
        c =      (-D*f)/(A*u + B*v + C*f)
        b =      (-D*v)/(A*u + B*v + C*f)
        a =      (-D*u)/(A*u + B*v + C*f)
        print("$({}, {})$ on image maps to $({:.0f}, {:.0f}, {:.0f})$ in camera coordinate 3D space.\\\\*".format(u,v,a,b,c))
        print("$({}, {})$ on image maps to $({:.0f}, {:.0f}, {:.0f})$ in meters in 3D space.\\\\*".format(u,v,a/10000,b/10000,c/10000))

    return
    

def q2(k):
    img = cv2.imread('images/IMG_20170207_195139_small.jpg')
    width  = img.shape[0]
    height = img.shape[1]
    flat_img  = img.reshape(width*height, 3)

    points = []
    while len(points) < k:
        p = [int(random.random() * img.shape[0]),
             int(random.random() * img.shape[1])]
        if p not in points:
            points.append(p)

    centers = np.array(img[[p[0] for p in points], [p[1] for p in points]])
    iteration = 0
    last_max_distance = np.inf
    while True:
        distances = scipy.spatial.distance.cdist(flat_img, centers, 'euclidean')
        max_dist = np.max(distances)
        delta = abs(max_dist - last_max_distance)
        if delta < 1.0:
            break
        last_max_distance = max_dist
        print("Iteration: {}. delta {}".format(iteration, delta))
        iteration += 1
        groups = np.argmin(distances, axis=1)
        centers = np.array([[np.average(flat_img[groups==i][:,0]),
                        np.average(flat_img[groups==i][:,1]),
                        np.average(flat_img[groups==i][:,2])]
                        for i in range(k)])
    cv2.imwrite("images/test_v1_k{}.png".format(k), np.array(np.round(centers)[groups], dtype=np.uint8).reshape(width, height, 3))
    return
    
def q2_2(k):
    img = cv2.imread('images/IMG_20170207_195139_small.jpg')
    width  = img.shape[0]
    height = img.shape[1]
    flat_img  = img.reshape(width*height, 3)

    flat_img_2 = np.float32([list(img[i,j]) + [i, j]
                               for i in range(width)
                               for j in range(height)])

    points = []
    while len(points) < k:
        p = [int(random.random() * img.shape[0]),
             int(random.random() * img.shape[1])]
        if p not in points:
            points.append(p)

    centers = np.array(img[[p[0] for p in points], [p[1] for p in points]])
    centers = []
    for p in points:
        centers.append(list(img[p[0], p[1]]) + list(p))
    iteration = 0
    last_max_distance = np.inf

    while True:
        distances = scipy.spatial.distance.cdist(flat_img_2, centers, 'euclidean')
        max_dist = np.max(distances)
        delta = abs(max_dist - last_max_distance)
        if delta < 1.0:
            break
        last_max_distance = max_dist
        print("Iteration: {}. delta {}".format(iteration, delta))
        iteration += 1
        groups = np.argmin(distances, axis=1)
        centers = np.array([[np.average(flat_img_2[groups==i][:,0]),
                             np.average(flat_img_2[groups==i][:,1]),
                             np.average(flat_img_2[groups==i][:,2]),
                             np.average(flat_img_2[groups==i][:,3]),
                             np.average(flat_img_2[groups==i][:,4]),
                            ]
                        for i in range(k)])
    centers = np.round(centers)
    color_centers = np.array([c[:3] for c in centers])
    cv2.imwrite("images/test_v2_k{}.png".format(k), np.array(color_centers[groups], dtype=np.uint8).reshape(width, height, 3))
    return

q1()

