import bresenham as br
import pdb
import numpy as np
import cv2
import math

def main():
    image = np.zeros((1000, 1000, 3), np.uint8)

    image[np.less(*np.meshgrid(range(1000), range(1000)))] = (255, 255, 255)

#    cv2.line(image, (0,0), (1000, 1000), (255,255,255), 1)


    cv2.imwrite('images/test_image_45.jpg', image)


if __name__=='__main__':
    main()
