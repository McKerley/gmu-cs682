
# CS682 homework 2
# Paul McKerley (G00616949)

import bresenham as br
from collections import defaultdict
import cv2
import math
import numpy as np
import os.path
from operator import itemgetter
import pdb
from pprint import pprint
import sys

def polar_to_cartesian(rho, theta, boundaries, multiplier):
    """
    Convert line in polar coordinates to cartesian. Code comes from opencv hough transform example found here:
    http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html
    """
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + multiplier*(-b))
    y1 = int(y0 + multiplier*(a))
    x2 = int(x0 - multiplier*(-b))
    y2 = int(y0 - multiplier*(a))
    ret_val, p1, p2 = cv2.clipLine((0,0,boundaries[1],boundaries[0]),(x1,y1),(x2,y2))
    
    if not ret_val:
        sys.stderr.write("Got false return value from clipLine for ({},{})->({},{})".format(x1, y1, x2, y2))

    return p1, p2

def least_squares_intersection(lines):

    """Find best intersection of multiple lines using least squares

    technique. Requires converting pairs points into a parametric version
    of the line to pass to numpy.linalg.lstsqr function. This version of
    the equations look like this:

    A1x + B1y + C1 = 0
    A2x + B2y + C2 = 0
    ...
    Anx + Bny + Cn = 0

    And then find (x,y) by passing

    [[A1, B1],
    [A2, B2],
    ...
    [An, Bn]]

    and 

    [C1, C2, ..., Cn] to numpy.linalg.lstsq.

    After calculating the center, try to eliminate effects of outliers
    by removing up to 1/6 of the farthest lines from the center and
    recalculate if the center moves less than one pixel then return the
    center early.
    """

    a = []
    b = []
    c = []
    for p1, p2 in lines:
        A, B, C = line(p1,p2)
        a.append(A)
        b.append(B)
        c.append(C)
    a = np.array(a)
    b = np.array(b)
    c = np.array(c)

    if len(a) == 0:
        return None

    distance_threshold = 1.0
    center = np.linalg.lstsq(np.column_stack((a,b)), c)[0]
    return (center[0], center[1])
    distances = np.abs((a * center[0]) + (b * center[1]) + c)/np.sqrt((a*a) + (b*b))
    keep = np.ones(a.shape, dtype=bool)
    maxval = np.inf
    last_center = None
    test_delta = True
    
    for i in range(int(len(lines)/6)):
        if test_delta and last_center is not None:
            intersection_delta = math.sqrt((center[0] -
                                            last_center[0])**2 +
                                           (center[1] - last_center[1])**2 )
            print("{}:{} => {}. delta={}".format(i, last_center, center, intersection_delta))
            if intersection_delta < distance_threshold:
                break
        distances = np.abs(((a * center[0]) + (b * center[1]) + c)/np.sqrt((a**2) + (b**2)))
        maxval = np.max(distances)
        keep = distances < maxval
        a = a[keep]
        b = b[keep]
        c = c[keep]
        if np.sum(keep) == 0:
            break
        
        last_center = center
        center = np.linalg.lstsq(np.column_stack((a, b)), c)[0]

    return (center[0], center[1])

def percent_valid_pixels(line):
    return int(100.0*np.sum(line)/line.shape[0])

def line_angle(p1, p2):
    return math.atan2(p2[1] - p1[1], p2[0] - p1[0])

half_pi    = math.pi/2
quarter_pi = math.pi/4

def pixels_ortho_to_line(p1, p2, deriv_x, deriv_y):
    """
    Find if a pixel is orthogonal to a line within a fudge-factor
    of pi/4 (as suggested by Zoran in class.
    """
    angle = line_angle(p1, p2)

    ortho_angle = (angle + half_pi)%math.pi
    bottom = ortho_angle - quarter_pi
    top    = ortho_angle + quarter_pi

    points = np.array(list(br.line_points(p1, p2)))
    xs = points[:,1]
    ys = points[:,0]

    derive_angles = np.arctan2(deriv_y[xs, ys], deriv_x[xs, ys])
    valid_pixels = np.logical_and(derive_angles >= bottom, derive_angles <= top)
    return valid_pixels, points

def pct_fill_line(p1, p2, deriv_x, deriv_y):
    """Find percentage of valid pixels in line"""
    valid_pixels, points = pixels_ortho_to_line(p1, p2, deriv_x, deriv_y)
    pct_fill = percent_valid_pixels(valid_pixels)

    return valid_pixels, points


def line(p1, p2):
    """
    Convert line between to points to a line equation in form Ax + Bx + C = 0

    Taken from this stackoverflow page:

    http://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines-in-python
    """

    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0]*p2[1] - p2[0]*p1[1])
    return A, B, -C

def intersection(L1, L2):
    """
    Find intersection between two line equations.

    Taken from this stackoverflow page:

    http://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines-in-python
    """
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x,y
    else:
        return False

def keep_line(p1, p2, deriv_x, deriv_y):
    """
    Determine whether to keep a line between points p1 and p2.
    deriv_x: Sobel gradients in x direction
    deriv_y: Sobel gradients in x direction

    Rules:
    1. Ignore lines where < 5% of the pixels are valid (within pi/4 of deriv line)
    2. Ignore lines that are either vertical or horizontal. They don't contribute much 
       to vanishing point, and can often skew it badly.
    """
    valid_pixels, points = pct_fill_line(p1, p2, deriv_x, deriv_y)
    if percent_valid_pixels(valid_pixels) < 5:
        return False, None, None

    angle = line_angle(p1, p2)
    if abs(abs(angle) - half_pi) < .1:
        return False, None, None

    if abs(angle) < 0.1:
        return False, None, None

    if abs(angle - math.pi) < 0.1:
        return False, None, None

    return True, valid_pixels, points

def main(image_file_names):
    
#    pdb.set_trace()
    SOBEL_SIZE = 3
    CANNY_MIN = 90
    CANNY_MAX = 200
    HOUGH_RHO_ACCURACY = 1
    HOUGH_THETA_ACCURACY = np.pi/360.0
    HOUGH_THRESHOLD  = 170
    HOUGHP_THRESHOLD = 100
    HOUGHP_MIN_LINE_LENGTH = 200
    HOUGHP_MAX_LINE_GAP = 10

    write_images = True

    for image_file_name in image_file_names:
        images = {}
        image_dir = '.'
        if '/' in image_file_name:
            image_dir = os.path.dirname(image_file_name)

        images = {}
        images['original']   = cv2.imread(image_file_name)
        images['gray'] = cv2.cvtColor(images['original'], cv2.COLOR_BGR2GRAY)

        images['canny_edges'] = cv2.Canny(images['gray'],CANNY_MIN, CANNY_MAX, apertureSize=SOBEL_SIZE)
        images['canny_loose_parameters'] = cv2.Canny(images['gray'],50, 200, apertureSize=SOBEL_SIZE)
        images['canny_tight_parameters'] = cv2.Canny(images['gray'],150, 300, apertureSize=SOBEL_SIZE)
        images['canny_sobel_5'] = cv2.Canny(images['gray'],1000, 1200, apertureSize=5)

        sobel_x = cv2.Sobel(images['canny_edges'], cv2.CV_64F, 1, 0, ksize=SOBEL_SIZE)
        sobel_y = cv2.Sobel(images['canny_edges'], cv2.CV_64F, 0, 1, ksize=SOBEL_SIZE)

        images['hough_lined'] = images['original'].copy()
        images['hough_lined_loose'] = images['original'].copy()
        images['hough_lined_tight'] = images['original'].copy()
        images['houghp_lined'] = images['original'].copy()
        images['houghp_lined_looser'] = images['original'].copy()
        multiplier = math.hypot(images['original'].shape[0], images['original'].shape[1])

        # Create sets of hough and houghp lines and apply same
        # filtering rules to each.
        line_image_sets = (
            ((polar_to_cartesian(l[0][0], l[0][1],
                                 images['original'].shape, multiplier)
                  for l in cv2.HoughLines(images['canny_edges'],
                                          HOUGH_RHO_ACCURACY,
                                          HOUGH_THETA_ACCURACY,
                                          HOUGH_THRESHOLD)),
              'hough_lined'),
            ((polar_to_cartesian(l[0][0], l[0][1],
                                 images['original'].shape, multiplier)
                  for l in cv2.HoughLines(images['canny_edges'],
                                          HOUGH_RHO_ACCURACY,
                                          HOUGH_THETA_ACCURACY,
                                          300)),
              'hough_lined_tight'),
            ((polar_to_cartesian(l[0][0], l[0][1],
                                 images['original'].shape, multiplier)
                  for l in cv2.HoughLines(images['canny_edges'],
                                          HOUGH_RHO_ACCURACY,
                                          HOUGH_THETA_ACCURACY,
                                          100)),
              'hough_lined_loose'),
            ((((l[0][0],l[0][1]),(l[0][2],l[0][3]))
                  for l in cv2.HoughLinesP(images['canny_edges'],
                                           HOUGH_RHO_ACCURACY,
                                           HOUGH_THETA_ACCURACY,
                                           HOUGHP_THRESHOLD,
                                           HOUGHP_MIN_LINE_LENGTH,
                                           HOUGHP_MAX_LINE_GAP,
                                               )),
              'houghp_lined'),
            ((((l[0][0],l[0][1]),(l[0][2],l[0][3]))
                  for l in cv2.HoughLinesP(images['canny_edges'],
                                           HOUGH_RHO_ACCURACY,
                                           HOUGH_THETA_ACCURACY,
                                           HOUGHP_THRESHOLD - 50,
                                           HOUGHP_MIN_LINE_LENGTH - 100,
                                           HOUGHP_MAX_LINE_GAP - 5,
                                               )),
              'houghp_lined_looser'),
            )

        for lines, image_name in line_image_sets:
            image_with_lines = images[image_name]
            image_with_filtered_lines = images[image_name].copy()
            print("Processing {} {}".format(image_file_name, image_name))
            all_lines = []
            all_pixels_and_points=[]
            for p1, p2 in lines:
                cv2.line(image_with_lines, p1, p2, (0,255,0), 2)

                # Filter the lines at this step
                passed, valid_pixels, points = keep_line(p1, p2, sobel_x, sobel_y)
                if passed:
                    all_pixels_and_points.append((valid_pixels, points))
                    cv2.line(image_with_filtered_lines, p1, p2, (0,255,0), 2)
                    all_lines.append((p1,p2))

            # Draw intersections of all filtered lines.
            all_x = []
            all_y = []
            image_with_intersections = image_with_filtered_lines.copy()
            for i, line_1 in enumerate(all_lines):
                for j, line_2 in enumerate(all_lines):
                    if j > i:
                        c = intersection(line(*line_1), line(*line_2))
                        if c:
                            if(c[0] >= 0 and c[0] < image_with_intersections.shape[0] and
                                c[1] >= 0 and c[1] < image_with_intersections.shape[1]):
                                all_x.append(c[0])
                                all_y.append(c[1])
                                cv2.circle(image_with_intersections, (int(c[0]),int(c[1])), 10, (0,0,255), 2)

            image_with_simple_vp = images['original'].copy()
            if len(all_x):
                vp_x = int(np.mean(all_x))
                vp_y = int(np.mean(all_y))
                print("{} VP: ({}, {})".format(image_name, vp_x, vp_y))
                cv2.circle(image_with_simple_vp, (vp_x, vp_y), 30, (255,0,255), 5)

            image_with_least_squares = images['original'].copy()
            center = least_squares_intersection(all_lines)
            if center:
                cv2.circle(image_with_least_squares, (int(center[0]),int(center[1])), 20, (255,0,0), 5)
            images[image_name + '_filtered_lines'] = image_with_filtered_lines
            images[image_name + '_intersections']  = image_with_intersections
            images[image_name + '_simple_vp']      = image_with_simple_vp
            images[image_name + '_least_squares']  = image_with_least_squares

        for image_name, image in images.items():
            images[image_name] = cv2.resize(image,
                                                (0,0),
                                                fx=('hough' in image_name and 0.4 or 0.75),
                                                fy=('hough' in image_name and 0.4 or 0.75))

        if write_images:
            base_name = image_file_name.replace('images','report')
            
            for image_name in ['original',
                                   'canny_edges',
                                   'canny_tight_parameters',
                                   'canny_loose_parameters',
                                   'canny_sobel_5',
                                   'houghp_lined',
                                   'houghp_lined_looser',
                                   'hough_lined',
                                   'hough_lined_loose',
                                   'hough_lined_tight',
                                   'houghp_lined_filtered_lines',
                                   'houghp_lined_looser_filtered_lines',
                                   'hough_lined_filtered_lines',
                                   'hough_lined_loose_filtered_lines',
                                   'hough_lined_tight_filtered_lines',
                                   'houghp_lined_intersections',
                                   'houghp_lined_looser_intersections',
                                   'hough_lined_intersections',
                                   'hough_lined_loose_intersections',
                                   'hough_lined_tight_intersections',
                                   'houghp_lined_least_squares',
                                   'houghp_lined_looser_least_squares',
                                   'hough_lined_least_squares',
                                   'hough_lined_loose_least_squares',
                                   'hough_lined_tight_least_squares',
                                   'houghp_lined_simple_vp',
                                   'houghp_lined_looser_simple_vp',
                                   'hough_lined_simple_vp',
                                   'hough_lined_loose_simple_vp',
                                   'hough_lined_tight_simple_vp',
                                   ]:
                cv2.imwrite(base_name.replace('.jpg', '_' + image_name + '.jpg'), images[image_name])
        else:
            cv2.imshow('original', images['original'])
            cv2.imshow('gray', images['gray'])
            cv2.imshow('canny_edges', images['canny_edges'])
            cv2.imshow('bad edges1 ', images['canny_tight_parameters'])
            cv2.imshow('bad edges2 ', images['canny_loose_parameters'])
            cv2.imshow('bad edges3 ', images['canny_sobel_5'])
            cv2.imshow('with houghp lines', images['houghp_lined'])
            cv2.imshow('with hough lines', images['hough_lined'])
            cv2.waitKey(0)
            cv2.destroyAllWindows()

def test(file_names):
    pdb.set_trace()
    main(file_names)
    
if __name__=='__main__':

    if len(sys.argv) > 1:
        main(sys.argv[1:])
    else:
        main(['images/ST2MainHall4028.jpg','images/ST2MainHall4035.jpg','images/ST2MainHall4040.jpg','images/ST2MainHall4045.jpg','images/ST2MainHall4048.jpg',])
