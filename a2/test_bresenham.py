import bresenham as br
import pdb
import numpy as np
import cv2
import math

def main():
    points = [
        ((0,0), (10,0)),
        ((0,0), (10,10)),
        ((0,0), (1,10)),
        ((0,0), (-1,10)),
        ((0,0), (-10,2)),
        ((0,0), (-10,-4)),
        ((0,0), (-10,-10)),
        ((0,0), (-1,-10)),
        ((0,0), (1,-10)),
        ((0,0), (10,-10)),
        ]
    for p1, p2 in points:
        print ("Slope for points ({}, {}) and ({}, {}) = {}".format(p1[0], p1[1], p2[0], p2[1], br.slope(p1, p2)))

    for p1, p2 in points:
        print ("Octant for points ({}, {}) and ({}, {}) = {}".format(p1[0], p1[1], p2[0], p2[1], br.line_octant(p1, p2)))
    
    points.extend([((0,0), (0,10)), ((0,0), (0,-10))])
    for p1, p2 in points:
#        pdb.set_trace()
        print ("Line for points ({}, {}) and ({}, {}) = {}".format(p1[0], p1[1], p2[0], p2[1], '->'.join(['({},{})'.format(p1,p2) for p1, p2 in br.line_points(p1, p2)])))
    
    image = np.zeros((640, 640, 3), np.uint8)

    colors = [
        (255, 0, 0),
        (0, 255, 0),
        (0, 0, 255),
        (255, 255, 255)
        ]
    color_names = [
        'blue',
        'green',
        'red',
        'white',
        ]

    color_idx = -1
    for angle in range(0, 360, 1):
        rad = math.radians(angle)
        color_idx = (color_idx + 1)%len(colors)
        color = colors[color_idx]
        p1 = (320, 320)
        p2 = (int(p1[0] + 300 * round(math.cos(rad), 10)), int(p1[1] + 300 * round(math.sin(rad), 10)))

#        print('color for angle {}, line {} -> {} = {}'.format(angle, p1, p2, color_names[color_idx]))
        for p in br.line_points(p1, p2):
            image[p[1], p[0]] = color

#    cv2.line(image, (0,0), (600, 600), (255,255,255), 10)

    cv2.imshow('Bresenham tests', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__=='__main__':
    main()
