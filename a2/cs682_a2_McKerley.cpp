#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

using namespace cv;
using namespace std;

void help()
{
 cout << "\nThis program demonstrates line finding with the Hough transform.\n"
         "Usage:\n"
         "./houghlines <image_name>, Default is pic1.jpg\n" << endl;
}

int main(int argc, char** argv)
{
 const char* filename = argc >= 2 ? argv[1] : "pic1.jpg";

 Mat src = imread(filename, 0);
 if(src.empty())
 {
     help();
     cout << "can not open " << filename << endl;
     return -1;
 }

 Mat dst, cdst;
 Canny(src, dst, 50, 200, 3);
 cvtColor(dst, cdst, CV_GRAY2BGR);

 vector<Vec2f> lines;
 const float multiplier = 2000.0;

 HoughLines(dst, lines, 1, CV_PI/180, 190, 0, 0 );
 for( size_t i = 0; i < lines.size(); i++ )
 {
     float rho = lines[i][0], theta = lines[i][1];
     Point pt1, pt2;
     double a = cos(theta), b = sin(theta);
     double x0 = a*rho, y0 = b*rho;
     pt1.x = cvRound(x0 + multiplier*(-b));
     pt1.y = cvRound(y0 + multiplier*(a));
     pt2.x = cvRound(x0 - multiplier*(-b));
     pt2.y = cvRound(y0 - multiplier*(a));
     line( cdst, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
 }

 vector<Vec4i> plines;
 HoughLinesP(dst, plines, 1, CV_PI/180, 50, 50, 10 );
 for( size_t i = 0; i < lines.size(); i++ )
 {
     Vec4i l = plines[i];
     line( cdst, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, CV_AA);
 }

 imshow("source", src);
 imshow("detected lines", cdst);

 waitKey();

 return 0;
}
