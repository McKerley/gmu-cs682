\documentclass[letter,8pt]{article}
\usepackage[margin=0.5in]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{color}
\usepackage{titlesec}
\usepackage{enumitem}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{parskip}
\usepackage{textcomp}
\usepackage{hyperref}
\titleformat{\section}
  {\normalfont\sffamily\Large\bfseries}
  {\thesection}{1em}{}

\begin{document}
\title{Computer Science 682 Assignment 2}
\author{Paul McKerley (G00616949)}
\fontfamily{phv}
\selectfont
\frenchspacing

\maketitle

\section*{Introduction}

This document describes how I completed assignment 2. The images can
be found at \url{http://www.mckerley.com/gmu/cs682/a2}. Username =
cs682. Password = InteriorsDislikesBillowy969.

I ran my code on Linux (Ubuntu 16.04.02) on a laptop with an Intel
i7. I used Python 3.5.2, and OpenCV 3.1.0. I should note that I used
Python Virtual Environments, which allows you to set up a custom
Python environment for a particular set of libraries and version of
Python. To do that I used this page:

\url{http://www.pyimagesearch.com/2015/07/20/install-opencv-3-0-and-python-3-4-on-ubuntu/}

The upshot is that I \textbf{import cv2} in my assignment code, which may
not work outside of a virtual environment, but it should be easy to
fix you need to run the code.

The source files included are cs682\_a2\_McKerley.py, and bresenham.py. The former is the main assignment,
and the latter is my implementation of the Bresenham algorithm as a separate module.

The images I picked were

\begin{enumerate}
  \item ST2MainHall4028.jpg
  \item ST2MainHall4035.jpg
  \item ST2MainHall4040.jpg
  \item ST2MainHall4045.jpg
  \item ST2MainHall4048.jpg
\end{enumerate}

They show samples from the sequence of similar pictures as the point
of view moves down the hallway. I think they show some interesting
effects of various features in the pictures. I will describes these
in greater detail in the Results section.

To save on the number of images, I overlaid the filtered lines, the
intersections, and the least squares best intersection on the same
image. The lines are green, and the pairwise intersection are green as
well. To show the least squares vanishing point, I used a thick,
wider, blue circle.

\section*{References}

I used various articles on the Internet to understand how to do some
of the techniques. Here are a list of web-pages from which I took code
or directly used the algorithms.

\begin{enumerate}
  \item To convert polar to cartesian coordinates: \url{http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html}
  \item To convert points to line equation, and to find line intersections: \url{http://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines-in-python}
  \item Bresenham: \url{https://www.cs.helsinki.fi/group/goa/mallinnus/lines/bresenh.html}
  \item Bresenham: \url{https://en.wikipedia.org/wiki/Bresenham's_line_algorithm}
  \item Least Squares Intersection of Lines. Johannes Traa. UIUC 2013.
  \item \url{https://en.wikipedia.org/wiki/Line-line_intersection\#In\_two\_dimensions\_2}
\end{enumerate}

\section*{Canny Edge Detection}

I found the thresholds for the call to Canny largely through trial and
error. For the thresholds I picked for hysteresis thresholding were 90
and 200. I found that these gave enough edges to work with for the
pictures I was doing. Making the thresholds more restrictive allowed
the strong painted line on the left side of the hallway to dominate
the vanishing point; while making them less restrictive cluttered the
image with too many edges, making a lot of random lines show up. I
left the default Sobel aperture as 3. Making it 5 again filled the
image with too much noise, even when setting the hysteresis parameters
much higher. For each picture the web-page has a set of samples of the
effect of using these thresholds, as well as the effect of using my
preferred thresholds.

\section*{Standard Hough Transform}

As with the Canny function, I picked parameters based on
experimentation. For testing tFor Hough I picked a threshold value of
170. I used images with the lines and intersections already overlaid
to help tune the Hough parameters. I also produced images showing a
looser parameter parameter, 100, and a tighter parameter, 300. In both
cases this led to the least squares vanishing point frequently being
misplaced, due either to too many lines or to few (in some cases of
the tighter parameter no lines appeared at all.)

In the case of the Hough Rho accuracy and Theta accuracy, I picked 1
and $\pi/360$, respectively. I did not experiment with these values too
much, since they seemed to make sense intuitively.

In the web-pages with the images, I compared the three parameter
choices side-by-side for comparison purposes.

\section*{Probabilistic Hough Transform}

The parameters to tune are threshold, min\_line\_length, and
max\_line\_gap. I found that a lower threshold than I used for
standard Hough worked better for Probablistic Hough. The tradeoffs are
similar. Make the likelihood of finding lines too high and you get a
lot of noise; make it too low and you get too few lines, dominated by
a single feature.

I compared to sets of parameters for pHough: one tighter and one
looser.  For the tighter ones, I set threshold=100, minLineLength=200,
maxLineGap=10; for the looser ones, threshold=50, minLineLength=100,
maxLineGap=5. It is clear from examining the pictures, that the looser
parameters get more, small, lines that the tighter one misses. Many of
them make it through the filtering step as well. The final result,
measured by the location of the least squares vanishing point, are
similar.

For each image, I placed the corresponding images for each step, and
with different parameters, beside each other, for easier comparison.

\section*{Filtering}

I used the following filtering technique:

\begin{enumerate}
  \item Discard horizontal and vertical lines. I did this because very
    few contribute to the vanishing point, and a preponderance of them
    on one side of the picture or another would skew the least-squares
    vanishing point in that direction.
  \item Use Bresenham algorithm, and the Sobel gradients, to determine
    if the line is orthoganal to each pixel, within $\lvert\pi/4\rvert$.
  \item Only keep lines whose percent of valid pixels is greater than $5\%$.
\end{enumerate}

\section*{Vanishing Points}

I plotted the vanishing points on an image from the image with the
lines they are associated with. I used a matrix technique from [2] in
the references.

We convert the 2 lines end-points to line equations,


$A_1x + B_1y + C_1 = 0$

$A_2x + B_2y + C_2 = 0$

and calculate the intersection $(x,y)$ like this,


$D  = A_1 * B_2 - B_1 * A_2$

$Dx = C_1 * B_2 - B_1 * C_2$

$Dy = A_1 * C_2 - C_1 * A_2$

$x = D_x / D$

$y = D_y / D$

I calculated the average of all the intersections that are in the image and
used those as the vanishing point. They are represented by a magenta circle
on the vanishing point image.

\section*{Least Squares Vanishing Point}

I spent a lot of time trying to figure out how to do this, but in the
end it was pretty easy.

For each line I expressed it as a line equation in the form

$Ax + By + C = 0$

I then used a numpy matrix with the $[A, B]$ values, and another with
the $C$ values, and used numpy.linalg.lstsq() to solve it.

So basically, I found the $(x,y)$ point for which the least sqaured
error to each line was minimized.

I also added some logic to iteratively remove the farthest line from
the vanishing point, until the change in the point was less than one
pixel, or I had removed $1/6$ of the lines, whichever came first. This
seemed to help somewhat in finding a better vanishing point. 

The results are plotted on the line images with a large blue circle.

\section*{Results}

The techniques used seemed to give pretty good locations for the
least-squared vanishing points, at least they seem to be visualy
plausible. I found the results to be a little better for the
probablistic Hough lines over the standard Hough transform. In
addition, the probablistic Hough lines algorithm is significantly
faster than the standard, which make it preferrable.

The pictures I choose showed some interesting results of various
specific features in the hallway, specifically the bulletin boards on
the left-hand wall, the diagonal, red, stripe painted on the wall,
and the presence in some pictures of people. 

In the first picture, ST2MainHall4028.jpg, the lines from the bulletin
boards and the hallway baseboards dominate the scene. There are
people, but they do not contribute much or interfere with the
lines. The least squares vanishing points for both Hough and
probabilistic Hough are pretty good; perhaps a little low.

In the next three pictures, ST2MainHall4035.jpg, ST2MainHall4040.jpg,
ST2MainHall4045.jpg, as the camera moves forward, the red stripe
has a more dominate effect, and its lines tend to displace the least
squares vanishing points to the right and up. 

In the final picture, ST2MainHall4048.jpg the bulletin boards again
diminish the effect of the red stripe, and the vanishing point again
centered much better.

The presence of people in the picture do not appear to have much
overall effect on the least-squares vanishing point.

\end{document}

\endinput
