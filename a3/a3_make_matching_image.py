# Paul McKerley
from collections import defaultdict
import cv2
from matplotlib import pyplot as plt
import numpy as np
import pdb
from scipy.cluster.hierarchy import dendrogram, linkage
import sys

# Function to write clusters out into and HTML file
def clusters_to_html(alg, clusters, all_file_names, file_name, cutoff):
    with open('output/{}_clusters.html'.format(alg), 'w') as out:
        out.write("<html><body>\n")
        out.write("<p><h1>Distance file {} -- cutoff distance {}</h1></p>\n".format(file_name, cutoff))
        for clust_num, cluster in enumerate(clusters):
            out.write("<p><h2>Cluster {}</h2></p><table><tr>\n".format(clust_num))
            for i, n in enumerate(cluster):
                if i > 0 and i%4 == 0:
                    out.write("</tr>\n")
                    if i < len(cluster):
                        out.write("<tr>\n")
                out.write('<td><img src="{}"><p>{}</p></td>'.format(all_file_names[n].replace('images', 'small_images'),
                              all_file_names[n].replace('images/', '')))
            if i%4 != 0:
                while i%4 != 0:
                    out.write("<td></td>\n")
                    i += 1
            out.write("</table>\n")
        out.write("</body></html>\n")

def main(alg, file_name, cutoff):
    # alg is the algorithm used -- orb, sift, or surf
    # file_name is the distance CSV file produced by a3_match_files.py
    # cutoff is the depth in the linkage tree to descend below which
    #   all images are considered to be in the same cluster.

    all_file_names = set()
    distances = defaultdict(lambda: defaultdict())

    with open(file_name) as f:
        for line in f:
            i1, i2, d = line.strip().split(',')
            all_file_names.add(i1)
            all_file_names.add(i2)
            distances[i1][i2] = float(d)

    all_file_names = sorted(all_file_names)

    # Create the matching image for all files.
    img = np.array([int(distances[x][y]*255.0) for x in all_file_names for y in all_file_names], dtype=np.uint8)
    img = img.reshape((len(all_file_names), len(all_file_names)))
    img = cv2.resize(img,(640, 640), interpolation = cv2.INTER_CUBIC)
    cv2.imwrite(file_name.replace('.csv', '.png'), img)

    # Calculate the hierarchical linkage
    distances = np.array([distances[x][y] for x in all_file_names for y in all_file_names], dtype=np.float)
    distances = distances.reshape((len(all_file_names), len(all_file_names)))
    upper_triangular_indices = np.triu_indices(distances.shape[0], k=1)
    Z = linkage(distances[upper_triangular_indices], 'ward')

    # Create and write the dendogram pdf file
    plt.figure(figsize=(25, 10))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('sample index')
    plt.ylabel('distance')
    dendrogram(
        Z,
        leaf_rotation=90.,  
        leaf_font_size=8.,  
        labels=all_file_names,
    )
    plt.savefig(file_name.replace('.csv', '_dendrogram.pdf'))

    # Use BFS to cluster images below cutoff
    queue = [len(Z) - 1]
    n_samples = len(all_file_names)
    clusters = []
    while queue:
        parent = queue.pop(0)
        a    = int(Z[parent][0])
        b    = int(Z[parent][1])
        dist = Z[parent][2]

        if dist <= cutoff:
            cluster = []
            sample_queue = [a, b]
            while sample_queue:
                sample = sample_queue.pop(0)
                if sample < n_samples:
                    cluster.append(sample)
                else:
                    sample -= n_samples
                    sample_queue.append(int(Z[sample][0]))
                    sample_queue.append(int(Z[sample][1]))
            clusters.append(cluster)
        else:
            queue.append(a - n_samples)
            queue.append(b - n_samples)

    # Write out clusters to HTML file
    clusters_to_html(alg, clusters, all_file_names, file_name, cutoff)

# Call
#   python3 a3_make_matching_image.py [orb|sift|surf] match_file_name.csv clustering_threshold

if __name__=='__main__':
    main(sys.argv[1], sys.argv[2], float(sys.argv[3]))
