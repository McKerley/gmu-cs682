# Paul McKerley
import cv2
import functools
import glob
import matplotlib.pyplot as plt
import numpy as np
import operator
import os
import pickle
import sys
import pdb
from collections import defaultdict

# Sample images to display
samples = [
    ('images/ST2MainHall4078.jpg', 'images/ST2MainHall4085.jpg'),
    ('images/ST2MainHall4078.jpg', 'images/ST2MainHall4093.jpg'),
    ('images/ST2MainHall4084.jpg', 'images/ST2MainHall4085.jpg'),
    ('images/ST2MainHall4084.jpg', 'images/ST2MainHall4086.jpg'),
    ('images/ST2MainHall4085.jpg', 'images/ST2MainHall4086.jpg'),
    ]
                
alg_distance_measure = {
    'sift': cv2.NORM_L2,
    'surf': cv2.NORM_L2,
    'orb':  cv2.NORM_HAMMING,
    }

alg_match_threshold = {
    'sift': 40,
    'surf': 0.03,
    'orb':  20,
    }

# Load keypoints and descriptors from file. They are
# stores as tuples and lists, so convert to cv2.Keypoint
# and numpy arrays, respectively.
def load_features(features_file_name):
    file_keypoint_desc = {}
    with open(features_file_name, 'rb') as input:
        data = pickle.load(input)
        for file_name, kps_desc in data.items():
                kps, desc = kps_desc
                kps = [
                    cv2.KeyPoint(
                            x=kp[0][0],
                            y=kp[0][1],
                            _size=kp[1],
                            _angle=kp[2], 
                            _response=kp[3],
                            _octave=kp[4],
                            _class_id=kp[5]) for kp in kps]
                desc = np.array(desc)
                file_keypoint_desc[file_name] = (kps, desc)
    return file_keypoint_desc

def bf_matcher(alg, desc_1, desc_2):
    bf = cv2.BFMatcher(alg_distance_measure[alg], crossCheck=True)
    matches = bf.match(desc_1, desc_2)
    matches = sorted(matches, key = lambda x:x.distance)
    return [m for m in matches if m.distance <= alg_match_threshold[alg]]

def bf_knn(alg, desc_1, desc_2):
    bf = cv2.BFMatcher()

    matches = bf.knnMatch(desc_1,desc_2, k=2)
    good_matches = [m for m,n in matches if m.distance < 0.75*n.distance]
    return good_matches

def main(alg, features_file_name):
    samples_only = False

    alg_matcher = {
        'orb': bf_matcher,
#        'sift': bf_knn,
#        'surf': bf_knn,
        'sift': bf_matcher,
        'surf': bf_matcher,
        }

    image_distances = defaultdict(lambda: defaultdict())
    max_samples = 100

    file_keypoint_desc = load_features(features_file_name)

    if not samples_only:
        all_file_names = sorted(file_keypoint_desc.keys())
    else:
        sample_file_names = set([x[0] for x in samples] + [x[1] for x in samples])
        all_file_names = sorted([x for x in file_keypoint_desc.keys() if x in sample_file_names])

    for file_name_1 in all_file_names:
        all_distances = []
        max_distance  = -1.0
        max_matches   = -1

        kp_1, desc_1 = file_keypoint_desc[file_name_1]
        print("\nImage {} has {} key points and {} descriptors".format(file_name_1, len(kp_1), len(desc_1)))

        for file_name_2 in all_file_names:
            if file_name_2 < file_name_1:
                continue
            sys.stdout.write('.')
            kp_2, desc_2 = file_keypoint_desc[file_name_2]

            good_matches = alg_matcher[alg](alg, desc_1, desc_2)

            for sample in samples:
                if file_name_1 == sample[0] and file_name_2 == sample[1]:
                    gray_1 = cv2.cvtColor(cv2.imread(file_name_1), cv2.COLOR_BGR2GRAY)
                    img_1  = cv2.drawKeypoints(gray_1,kp_1,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
                    gray_2 = cv2.cvtColor(cv2.imread(file_name_2), cv2.COLOR_BGR2GRAY)
                    img_2  = cv2.drawKeypoints(gray_2,kp_2,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
                    print("drawing matches for {} images {} and {}".format(alg, file_name_1, file_name_2))
                    match_image = cv2.drawMatches(img_1, kp_1, img_2, kp_2, good_matches, None, flags=2)
                    output_file_name = 'output/{}_{}_{}_matches.png'.format(file_name_1.replace('images/', '').replace('.jpg', ''),
                                                                     file_name_2.replace('images/', '').replace('.jpg', ''),
                                                                     alg)
                    print("writing sample to {}".format(output_file_name))
                    cv2.imwrite(output_file_name, match_image)

            distances = [x.distance for x in good_matches]
            if distances:
                max_distance = max(max_distance, max(distances))

            if file_name_1 != file_name_2:
                max_matches = max(max_matches, len(distances))
            all_distances.append(distances)

        # Calculate normalized image distances
        if all_distances:
            miss_distance = max_distance + 1.0
            for i, distances in enumerate(all_distances):
                distances.extend([miss_distance for _ in range(max_matches-len(distances))])
            idx = 0
            divisor = sum((miss_distance for x in range(max_matches)))
            if int(divisor) == 0:
                divisor = 1.0
            for file_name_2 in (fn for fn in all_file_names if fn >= file_name_1):
                if len(all_distances[idx]) == 0:
                    distance = 1.0
                else:
                    distance = sum(all_distances[idx])/divisor
                image_distances[file_name_1][file_name_2] = image_distances[file_name_2][file_name_1] = distance
                idx += 1

    if not samples_only:
        with open("output/{}_image_distances.csv".format(alg), "w") as out:
            for file_name_1 in all_file_names:
                for file_name_2 in all_file_names:
                    out.write('{},{},{}\n'.format(file_name_1, file_name_2, image_distances[file_name_1][file_name_2]))

# Call
#   python3 a3_match_images.py [orb|sift|surf] features_file_name.pickle
if __name__=='__main__':
    main(sys.argv[1], sys.argv[2])
