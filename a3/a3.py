import cv2
import functools
import glob
import json
import matplotlib.pyplot as plt
import multiprocessing as mp
import numpy as np
import operator
import os
import sys
import pdb
from collections import defaultdict


def find_sift_features(gray, make_keypoint_image=False, verbose=False):
    sift = cv2.xfeatures2d.SIFT_create()
    kp_sift, des_sift = sift.detectAndCompute(gray,None)
    if verbose:
        print("Number of SIFT keypoints: {}".format(len(kp_sift)))

    sift_img = None
    if make_keypoint_image:
        sift_img=cv2.drawKeypoints(gray,kp_sift,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    return kp_sift, des_sift, sift_img

def find_surf_features(gray, make_keypoint_image=False, verbose=False):
    surf = cv2.xfeatures2d.SURF_create(500)
    kp_surf, des_surf = surf.detectAndCompute(gray,None)
    if verbose:
        print("Number of SURF keypoints: {}".format(len(kp_surf)))

    surf_img = None
    if make_keypoint_image:
        surf_img=cv2.drawKeypoints(gray,kp_surf,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    return kp_surf, des_surf, surf_img

def find_orb_features(gray, make_keypoint_image=False, verbose=False):
    orb = cv2.ORB_create(1000)
    kp_orb, des_orb = orb.detectAndCompute(gray,None)
    if verbose:
        print("Number of ORB keypoints: {}".format(len(kp_orb)))

    orb_img = None
    if make_keypoint_image:
        orb_img=cv2.drawKeypoints(gray,kp_orb,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    return kp_orb, des_orb, orb_img

def find_keypoints(file_names, write=False, show=False, algs=[]):
    all_kps = defaultdict(list)
    all_des = defaultdict(list)

    functions = {
        'sift': find_sift_features,
        'surf': find_surf_features,
        'orb':  find_orb_features,
    }

    for file_name in file_names:
        img = cv2.imread(file_name)
        gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        for alg in algs:
            kps, des, kp_image = functions[alg](gray, show or write)

            all_kps[alg].append(kps)
            all_des[alg].append(des)

            if write:
                cv2.imwrite(file_name.replace('.jpg', '.{}_keypoints.jpg'.format(alg.upper())), kp_image)
            if show:
                cv2.imshow('{} keypoints for {}'.format(alg.upper(), file_name), kp_image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

    return all_kps, all_des

def match_distance(max_value, max_samples, matches):
    if len(matches) < max_samples:
        for i in range(max_samples - len(matches)):
            matches.append(max_value)
    return functools.reduce(operator.mul, [match/max_value for match in matches[:max_samples]])*255.0

file_keypoint_desc = {}

def get_file_keypoint_desc(feature_detector, file_name):
    if file_name not in file_keypoint_desc:
        gray = cv2.cvtColor(cv2.imread(file_name), cv2.COLOR_BGR2GRAY)
        kp, desc = feature_detector.detectAndCompute(gray,None)
        file_keypoint_desc[file_name] = (kp, desc)
    return file_keypoint_desc[file_name]

def main(directory, alg, show=False, write=False, verbose=False):
    alg_constructor = {
        'sift': (cv2.xfeatures2d.SIFT_create, []),
        'surf': (cv2.xfeatures2d.SURF_create, [500]),
        'orb':  (cv2.ORB_create, [1000]),
    }

    alg_distance_measure = {
        'sift': cv2.NORM_L2,
        'surf': cv2.NORM_L2,
        'orb':  cv2.NORM_HAMMING,
    }

    alg_match_threshold = {
        'sift': 40,
        'surf': 0.04,
        'orb':  20,
    }

    image_distances = defaultdict(lambda: defaultdict())
    max_samples = 100

    all_file_names = sorted(glob.glob(os.path.join(directory, '*.jpg')))#[:20]
    feature_detector = alg_constructor[alg][0](*alg_constructor[alg][1])
    bf = cv2.BFMatcher(alg_distance_measure[alg], crossCheck=True)
#    pdb.set_trace()

    for file_name_1 in all_file_names:
        all_distances = []
        max_distance  = -1.0
        max_matches   = -1

        kp_1, desc_1 = get_file_keypoint_desc(feature_detector, file_name_1)
        print("\nImage {} has {} key points and {} descriptors".format(file_name_1, len(kp_1), len(desc_1)))

        for file_name_2 in all_file_names:
            if file_name_2 < file_name_1:
                continue
            sys.stdout.write('.')
            kp_2, desc_2 = get_file_keypoint_desc(feature_detector, file_name_2)
            if verbose:
                print("Image {} has {} key points and {} descriptors".format(file_name_2, len(kp_2), len(desc_2)))

            matches = bf.match(desc_1, desc_2)

            matches = sorted(matches, key = lambda x:x.distance)
#            distances = [x.distance for x in matches if x.distance <= alg_match_threshold[alg]]
            distances = [x.distance for x in matches]
            if distances:
                max_distance = max(max_distance, max(distances))
            distances = [d for d in distances if d <= alg_match_threshold[alg]][:100]
            if file_name_1 != file_name_2:
                max_matches = max(max_matches, len(distances))
            all_distances.append(distances)
            if show:
                match_image = cv2.drawMatches(gray_1, kp_1, gray_2, kp_2, good_matches, None, flags=2)
                plt.imshow(match_image)
                plt.show()
        miss_distance = max_distance + 1.0
        for i, distances in enumerate(all_distances):
            pass
            distances.extend([miss_distance for _ in range(max_matches-len(distances))])
            pass
        idx = 0
        divisor = sum((miss_distance for x in range(max_matches)))
        if int(divisor) == 0:
            divisor = 1.0
        for file_name_2 in (fn for fn in all_file_names if fn >= file_name_1):
            image_distances[file_name_1][file_name_2] = image_distances[file_name_2][file_name_1] = sum(all_distances[idx])/divisor
            idx += 1

    with open("{}_image_distances.csv".format(alg), "w") as out:
        for file_name_1 in all_file_names:
            for file_name_2 in all_file_names:
                out.write('{},{},{}\n'.format(file_name_1, file_name_2, image_distances[file_name_1][file_name_2]))

if __name__=='__main__':
    main(sys.argv[2], alg=sys.argv[1], show=False)
