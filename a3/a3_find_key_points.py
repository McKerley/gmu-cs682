# Paul McKerley 
import cv2
import functools
import glob
import json
import matplotlib.pyplot as plt
import multiprocessing as mp
import numpy as np
import operator
import os
import pdb
import pickle
import sys
from collections import defaultdict

# Different "constructors" for different feature detectors
alg_constructor = {
    'sift': (cv2.xfeatures2d.SIFT_create, []),
    'surf': (cv2.xfeatures2d.SURF_create, [500]),
    'orb':  (cv2.ORB_create, [1000]),
}

samples = ['4040']

def get_file_keypoint_desc(alg, file_name):
    # create feature detector based on passed-in algorithm

    func, args = alg_constructor[alg]
    feature_detector = func(*args)
    gray = cv2.cvtColor(cv2.imread(file_name), cv2.COLOR_BGR2GRAY)
    kp, desc = feature_detector.detectAndCompute(gray,None)

    for sample in samples:
        if sample in file_name:
            sample_file_name = file_name.replace('images', 'output').replace('.jpg', '_{}_features.png'.format(alg))
            img=cv2.drawKeypoints(gray,kp,None,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            cv2.imwrite(sample_file_name, img)

    # pickle has problems with cv2 objects, so save them as tuples
    kp = [(k.pt, k.size, k.angle, k.response, k.octave, k.class_id) for k in kp]

    # pickle also has problems with numpy arrays, so save as lists
    desc = [[x for x in y] for y in desc]

    return file_name, kp, desc

def main(alg, show=False, write=False, verbose=False):
    directory = 'images'

    if not os.path.exists('output'):
        os.mkdir('output')

    all_file_names = sorted(glob.glob(os.path.join(directory, '*.jpg')))

    file_keypoint_desc = {}

    # Use multiprocessing to calculate features in parallel
    with mp.Pool(None) as p:
        for fn, kp, desc in p.starmap(get_file_keypoint_desc,
                                      [(alg, file_name) for file_name in all_file_names]):
            file_keypoint_desc[fn] = (kp, desc)

    # save features to file
    with open('output/{}_descriptors.pickle'.format(alg), 'wb') as o:
        pickle.dump(file_keypoint_desc, o)

# Call
# python a3_find_key_points.py [orb|surf|sift] 
if __name__=='__main__':
    main(sys.argv[1])
