fig_files       := $(wildcard *.fig)
pdf_t_files     := $(patsubst %.fig, %.pdf_t, $(fig_files))

%.pdf_t : %.fig
	fig2dev -L pdftex $< $(patsubst %.fig, %.pdf, $<)
	fig2dev -L pdftex_t -p $(patsubst %.fig, %.pdf, $<) $< $@

%.pdf: %.tex
	pdflatex -interaction batchmode -halt-on-error $<
	pdflatex -interaction batchmode -halt-on-error $< # run twice to get cross-references right

.PHONY: clean
clean:
	rm *.aux *.log *~ 
