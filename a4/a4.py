#!/usr/bin/env python
import argparse
from collections import namedtuple
import cv2
import math
import numpy as np
import os
import pdb
import re

extract_file_base_name_re = re.compile(r'.*/([^/]+).JPG')
                   
def print_latex_matrix(M):
  print("""
\\[
  \\begin{bmatrix}""")
  for r in M:
    print("    {} \\\\".format(" & ".join(["${}$".format(x) for x in r])))
  print("""\\end{bmatrix}
\\]""")
  

# Find features using ORB and find matches between two images using
# Brute Force matcher. Limit matches to those with distance <= match_distance.
def find_and_match_features(image_1, image_2, orb_nfeatures, match_distance):
    gray_1 = cv2.cvtColor(image_1, cv2.COLOR_BGR2GRAY)
    gray_2 = cv2.cvtColor(image_2, cv2.COLOR_BGR2GRAY)
    feature_detector = cv2.ORB_create(orb_nfeatures)
    kp_1, desc_1 = feature_detector.detectAndCompute(gray_1,None)
    kp_2, desc_2 = feature_detector.detectAndCompute(gray_2,None)
    
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = [m for m in bf.match(desc_1, desc_2,) if m.distance <= match_distance]
    print("Got {} matches".format(len(matches)))

    return matches, kp_1, desc_1, kp_2, desc_2

def compute_transformation(image_1, image_2, points_1, points_2):
    h, status = cv2.findHomography(points_1, points_2)

# Find the outer bounds of an image after a homography transformation has been applied to it.
# Returns edge positions (left, top, bottom, right) and size of image.

def find_warped_image_size(image_1, image_2, homography):
    # figure out size of merged image by applying the homography to
    # the corners of image to be transformed. 
    corners = np.float32(
        [[0,0],                                             # TL
         [image_1.shape[1] - 1.0, 0],                       # TR
         [0, image_1.shape[0] - 1.0],                       # LL
         [image_1.shape[1] - 1.0, image_1.shape[0] - 1.0]]  # LR
        )

    hcorners = cv2.convertPointsToHomogeneous(corners).reshape(-1, 3)
    transformed_corners = np.float32([
        homography.dot(hcorners[0]),
        homography.dot(hcorners[1]),
        homography.dot(hcorners[2]),
        homography.dot(hcorners[3]),
        ])

#    pdb.set_trace()
    transformed_corners = cv2.convertPointsFromHomogeneous(transformed_corners)
    transformed_corners = transformed_corners.reshape(4,2)
    left   = min(transformed_corners[0][0], transformed_corners[2][0], 0.0)
    top    = min(transformed_corners[0][1], transformed_corners[1][1], 0.0)
    right  = max(transformed_corners[1][0], transformed_corners[3][0], image_2.shape[1])
    bottom = max(transformed_corners[2][1], transformed_corners[3][1], image_2.shape[0])

    width = right - left
    height = bottom - top

    print("Corners {}\nchanged to\n{}\n\n".format(corners.astype(int), transformed_corners.astype(int)))
    print("left = {}".format(left))
    print("top = {}".format(top))
    print("right = {}".format(right))
    print("bottom = {}".format(bottom))
    print("width = {}".format(width))
    print("height = {}".format(height))

    return top, left, bottom, right, (math.ceil(width), math.ceil(height))

# Code to perform section 

def merge_images(file_names, orb_nfeatures, match_distance, write_files, scale, merge_forward, weighted_blending):
    if len(file_names) < 2:
        raise Exception("will only merge 2 or more images")

    image_1 = None
    image_2 = None
    merged_image = None
    all_file_base_names = []
    for file_name in file_names:
        file_base_name = extract_file_base_name_re.sub(r'\1', file_name)
        all_file_base_names.append(file_base_name)
        if image_1 is None:
            image_1 = cv2.imread(file_name)
            print("Read {}".format(file_name))
            continue
        elif image_2 is None:
            image_2 = cv2.imread(file_name)
            print("Read {}".format(file_name))
        else:
            if merge_forward:
                image_1 = merged_image
                image_2 = cv2.imread(file_name)
            else:
                image_2 = merged_image
                image_1 = cv2.imread(file_name)
            print("Read {}".format(file_name))
            

        matches, kp_1, desc_1, kp_2, desc_2 = find_and_match_features(image_1, image_2, orb_nfeatures, match_distance)
        sorted_matches = sorted(matches, key=lambda x: x.distance)

        points_1 = np.float32([np.float32(kp_1[m.queryIdx].pt) for m in sorted_matches])
        points_2 = np.float32([np.float32(kp_2[m.trainIdx].pt) for m in sorted_matches])

        np.reshape(points_1, (-1,2))
        np.reshape(points_2, (-1,2))
        
        homography_args = dict(method=cv2.RANSAC,
                               ransacReprojThreshold=1.00,
                               maxIters=20000,
                               confidence=0.999)
        result = cv2.findHomography(points_1, points_2, **homography_args)

        if result is not None:
            homography, status = result
        else:
            raise Exception("Bad return from findHomography")

        if status is None:
            raise Exception("Failed to compute homography")

        
        flag_map = {0: 'DEFAULT',
                    8: 'RANSAC',
                    4: 'LMEDS',
                    }
        print("method: {}, ransaceReprojThreshold: {}, maxIters: {}, confidence: {}".format(
            flag_map[homography_args['method']], homography_args['ransacReprojThreshold'],
            homography_args['maxIters'], homography_args['confidence']))
        print_latex_matrix(homography)

        top, left, bottom, right, merge_image_size = find_warped_image_size(image_1, image_2, homography)

        # translate destination image
        left_adjust = left < 0 and -left or 0
        top_adjust = top < 0 and -top or 0
        M = np.float32([[1, 0, left_adjust], [0, 1, top_adjust]])
        image_2_shifted = cv2.warpAffine(image_2, M, merge_image_size)

        # warp source image. Multiply by translation matrix to make sure images align.
        M = np.float32([[1, 0, left_adjust], [0, 1, top_adjust], [0, 0, 1]])
        image_1_warped = cv2.warpPerspective(image_1, homography.dot(M), merge_image_size)

        # Merge images. Make sure each each image contributes an equal weight to the final merge.
        if weighted_blending:
            alpha = 1.0/len(all_file_base_names)
        else:
            alpha = .5
        print(alpha)
        merged_image = cv2.addWeighted(image_1_warped, alpha, image_2_shifted, 1.0 - alpha, 0.0)

        if scale != 1.0:
            merged_image_display = cv2.resize(merged_image, (0,0), fx=scale, fy=scale)
        else:
            merged_image_display = merged_image

        if write_files:
            output_file_name = 'images/{}_{}_{}_{}_{}_merged.png'.format('-'.join(all_file_base_names),
                                                                 homography_args['method'],
                                                                 int(homography_args['ransacReprojThreshold']),
                                                                 homography_args['maxIters'],
                                                                 int(1000*homography_args['confidence']),
                               )

            cv2.imwrite(output_file_name, merged_image_display)
        else:
            # cv2.imshow('Image 1',         cv2.resize(image_1,  (0,0), fx=scale, fy=scale))
            # cv2.imshow('Image 2',         cv2.resize(image_2, (0,0), fx=scale, fy=scale))
            # cv2.imshow('Image 1 Warped',  cv2.resize(image_1_warped,  (0,0), fx=scale, fy=scale))
            # cv2.imshow('Image 2 Shifted', cv2.resize(image_2_shifted, (0,0), fx=scale, fy=scale))
            cv2.imshow('Merge Image {}'.format(', '.join(all_file_base_names)), merged_image_display)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

def compare_two_images(file_names, orb_nfeatures, match_distance, write_files, scale):
    if len(file_names) != 2:
        raise Exception("Must call compare_two_images with exactly 2 filenames")
    image_1 = cv2.imread(file_names[0])
    image_2 = cv2.imread(file_names[1])
    matches, kp_1, desc_1, kp_2, desc_2 = find_and_match_features(image_1, image_2, orb_nfeatures, match_distance)
    sorted_matches = sorted(matches, key=lambda x: x.distance)

    match_image = cv2.drawMatches(image_1, kp_1, image_2, kp_2, sorted_matches, None, flags=2|4)

    points_1 = np.float32([np.float32(kp_1[m.queryIdx].pt) for m in sorted_matches])
    points_2 = np.float32([np.float32(kp_2[m.trainIdx].pt) for m in sorted_matches])

    np.reshape(points_1, (-1,2))
    np.reshape(points_2, (-1,2))

    transform = cv2.estimateRigidTransform(points_1, points_2, True)

    print_latex_matrix(transform)
    
    transformed_image = cv2.warpAffine(image_1, transform, (image_2.shape[1],image_2.shape[0]))

    homography, status = cv2.findHomography(points_1, points_2, method=cv2.RANSAC)

    print_latex_matrix(homography)

    top, left, bottom, right, merge_image_size = find_warped_image_size(image_1, image_2, homography)

    left_adjust = left < 0 and -left or 0
    top_adjust = top < 0 and -top or 0
    M = np.float32([[1, 0, top_adjust], [0, 1, left_adjust], [0, 0, 1]])
#    pdb.set_trace()
    warped_image = cv2.warpPerspective(image_1, homography.dot(M), merge_image_size)
    
    file_name_1 = extract_file_base_name_re.sub(r'\1', file_names[0], )
    file_name_2 = extract_file_base_name_re.sub(r'\1', file_names[1], )

    if scale != 1.0:
        match_image       = cv2.resize(match_image, (0,0), fx=scale, fy=scale)
        transformed_image = cv2.resize(transformed_image, (0,0), fx=scale, fy=scale)
        warped_image      = cv2.resize(warped_image, (0,0), fx=scale, fy=scale)

    if write_files:
        matches_image_name   = 'images/{}_to_{}_matches.png'.format(file_name_1, file_name_2)
        transform_image_name = 'images/{}_to_{}_transform.png'.format(file_name_1, file_name_2)
        warp_image_name      = 'images/{}_to_{}_warp.png'.format(file_name_1, file_name_2)
        cv2.imwrite(matches_image_name, match_image)
        cv2.imwrite(transform_image_name, transformed_image)
        cv2.imwrite(warp_image_name, warped_image)
    else:
        matches_image_name   = '{}_to_{}_matches'.format(file_name_1, file_name_2)
        transform_image_name = '{}_to_{}_transform'.format(file_name_1, file_name_2)
        warp_image_name      = '{}_to_{}_warp'.format(file_name_1, file_name_2)
        cv2.imshow(file_name_1, cv2.resize(image_1, (0,0), fx=scale, fy=scale))
        cv2.imshow(file_name_2, cv2.resize(image_2, (0,0), fx=scale, fy=scale))
        cv2.imshow(matches_image_name, match_image)
        cv2.imshow(transform_image_name, transformed_image)
        cv2.imshow(warp_image_name, warped_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
def main(file_names, orb_nfeatures, match_distance, write_files, scale, merge_forward, weighted_blending, compare):
    if compare:
        compare_two_images(file_names, orb_nfeatures, match_distance, write_files, scale)
    else:
        merge_images(file_names, orb_nfeatures, match_distance, write_files, scale, merge_forward, weighted_blending)        

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--files', '-f',
                        action='append',
                        help='Image files to process.',
                        default=None)
    parser.add_argument('--orb_nfeatures',
                        help='Number of features for ORB to attempt to find.',
                            type=int,
                        default=1000)
    parser.add_argument('--match_distance',
                        help='Maximum distance to allow between ORB features.',
                        type=float,
                        default=20.0)
    parser.add_argument('--write', '-w',
                        action='store_true',
                        help='Write images to file instead of displaying them.',
                        default=False)
    parser.add_argument('--merge_forward',
                        action='store_true',
                        help='Merge previous images into new one. Otherwise do the reverse.',
                        default=False)
    parser.add_argument('--compare',
                        action='store_true',
                        help='Compare two images with each other. If false well merge a series of two or more images.',
                        default=False)
    parser.add_argument('--weighted_blending',
                        action='store_true',
                        help='Reduce alpha while blending so each image is equally represented in final image. Otherwise blend equally.',
                        default=False)
    parser.add_argument('--scale', '-s',
                        metavar='F',
                        help='Scale frame by F before displaying or writing.',
                        type=float,
                        default=1.0)
    arg = parser.parse_args()
    main(arg.files, arg.orb_nfeatures, arg.match_distance, arg.write, arg.scale, arg.merge_forward, arg.weighted_blending, arg.compare)
    
