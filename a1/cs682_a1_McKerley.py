
# CS682 homework 1
# Paul McKerley (G00616949)

import cv2
import numpy as np
import os.path
import sys
from matplotlib import pyplot as plt
import pdb
from collections import defaultdict

def main(image_file_name):
    image_dir = '.'
    if '/' in image_file_name:
        image_dir = os.path.dirname(image_file_name)

    images = {}
    original = cv2.imread(image_file_name)
    
    # Crop out central 480x640 region of the image
    cropped_h = 480
    cropped_w = 640
    h, w, _   = original.shape
    new_y     = (h - cropped_h)//2
    new_x     = (w - cropped_w)//2

    images['rgb']  = original[new_y:new_y+cropped_h, new_x:new_x+cropped_w]

    # Convert image to grayscale
    images['gray']     = cv2.cvtColor(images['rgb'], cv2.COLOR_BGR2GRAY)

    # Apply median filter to RGB and grayscale images
    for base in ['rgb', 'gray']:
        for width in [3,5]:
            key = '{}_{}_{}'.format(base, 'median', width)
            images[key] = cv2.medianBlur(images[base], width)

    # Apply Gaussian filter to RGB an grayscale images
    for base in ['rgb', 'gray']:
        for sigma in [1,2,3]:
            key = '{}_{}_{}'.format(base, 'gaussian', sigma)
            images[key] = cv2.GaussianBlur(images[base], (0,0), sigma)

    # Apply DX, DY, and Magnitude (combined dx and dy filters) to all images
    new_images = {}
    identity = np.array([[0, 1, 0]]).reshape((3,1))
    for width in [3,5]:
        dx, dy = cv2.getDerivKernels(1, 1, width, normalize=False)
        for name, image in images.items():
            dx_key = '{}_dx_{}'.format(name, width)

            # Split image into channels, applit derivative filters, then merge
            new_images[dx_key] = cv2.merge([cv2.sepFilter2D(channel, -1, dx, identity) for channel in cv2.split(image)])
            dy_key = '{}_dy_{}'.format(name, width)
            new_images[dy_key] = cv2.merge([cv2.sepFilter2D(channel, -1, dy, identity) for channel in cv2.split(image)])
            key = '{}_dxdy_{}'.format(name, width)
            new_images[key] = cv2.merge([cv2.sepFilter2D(channel, -1, dx, dy) for channel in cv2.split(image)])

    images.update(new_images)

    # Build tables of file names for output to web page
    links = {}
    sections = ['Unfiltered']
    links['Unfiltered'] = (['RGB', 'Grayscale'], [['rgb.png', 'gray.png']])

    sections.append('Median')
    links['Median'] = (['Width', 'RGB', 'Grayscale'],
                           [['3', 'rgb_median_3.png', 'gray_median_3.png'],
                            ['5', 'rgb_median_5.png', 'gray_median_5.png'],])

    sections.append('Gaussian')
    links['Gaussian'] = (['Sigma', 'RGB', 'Grayscale'],
                           [['1', 'rgb_gaussian_1.png', 'gray_gaussian_1.png'],
                            ['2', 'rgb_gaussian_2.png', 'gray_gaussian_2.png'],
                            ['3', 'rgb_gaussian_3.png', 'gray_gaussian_3.png'],
                           ])

    sections.append('DX')
    links['DX'] = (['Filter', 'Filter Parameter (none/width/sigma)', 'Derivative Width', 'RGB', 'Grayscale'],
                                [['None', '', '3', 'rgb_dx_3.png', 'gray_dx_3.png'],
                                 ['None', '', '5', 'rgb_dx_5.png', 'gray_dx_5.png'],
                                 ['Median', '3', '3', 'rgb_median_3_dx_3.png', 'gray_median_3_dx_3.png'],
                                 ['Median', '3', '5', 'rgb_median_3_dx_5.png', 'gray_median_3_dx_5.png'],
                                 ['Median', '5', '3', 'rgb_median_5_dx_3.png', 'gray_median_5_dx_3.png'],
                                 ['Median', '5', '5', 'rgb_median_5_dx_5.png', 'gray_median_5_dx_5.png'],
                                 ['Gaussian', '1', '3', 'rgb_gaussian_1_dx_3.png', 'gray_gaussian_1_dx_3.png'],
                                 ['Gaussian', '1', '5', 'rgb_gaussian_1_dx_5.png', 'gray_gaussian_1_dx_5.png'],
                                 ['Gaussian', '2', '3', 'rgb_gaussian_2_dx_3.png', 'gray_gaussian_2_dx_3.png'],
                                 ['Gaussian', '2', '5', 'rgb_gaussian_2_dx_5.png', 'gray_gaussian_2_dx_5.png'],
                                 ['Gaussian', '3', '3', 'rgb_gaussian_3_dx_3.png', 'gray_gaussian_3_dx_3.png'],
                                 ['Gaussian', '3', '5', 'rgb_gaussian_3_dx_5.png', 'gray_gaussian_3_dx_5.png'],
                                 ])

    sections.append('DY')
    links['DY'] = (['Filter', 'Filter Parameter (none/width/sigma)', 'Derivative Width', 'RGB', 'Grayscale'],
                                [['None', '', '3', 'rgb_dy_3.png', 'gray_dy_3.png'],
                                 ['None', '', '5', 'rgb_dy_5.png', 'gray_dy_5.png'],
                                 ['Median', '3', '3', 'rgb_median_3_dy_3.png', 'gray_median_3_dy_3.png'],
                                 ['Median', '3', '5', 'rgb_median_3_dy_5.png', 'gray_median_3_dy_5.png'],
                                 ['Median', '5', '3', 'rgb_median_5_dy_3.png', 'gray_median_5_dy_3.png'],
                                 ['Median', '5', '5', 'rgb_median_5_dy_5.png', 'gray_median_5_dy_5.png'],
                                 ['Gaussian', '1', '3', 'rgb_gaussian_1_dy_3.png', 'gray_gaussian_1_dy_3.png'],
                                 ['Gaussian', '1', '5', 'rgb_gaussian_1_dy_5.png', 'gray_gaussian_1_dy_5.png'],
                                 ['Gaussian', '2', '3', 'rgb_gaussian_2_dy_3.png', 'gray_gaussian_2_dy_3.png'],
                                 ['Gaussian', '2', '5', 'rgb_gaussian_2_dy_5.png', 'gray_gaussian_2_dy_5.png'],
                                 ['Gaussian', '3', '3', 'rgb_gaussian_3_dy_3.png', 'gray_gaussian_3_dy_3.png'],
                                 ['Gaussian', '3', '5', 'rgb_gaussian_3_dy_5.png', 'gray_gaussian_3_dy_5.png'],
                                 ])

    sections.append('Magnitude')
    links['Magnitude'] = (['Filter', 'Filter Parameter (none/width/sigma)', 'Derivative Width', 'RGB', 'Grayscale'],
                                [['None', '', '3', 'rgb_dxdy_3.png', 'gray_dxdy_3.png'],
                                 ['None', '', '5', 'rgb_dxdy_5.png', 'gray_dxdy_5.png'],
                                 ['Median', '3', '3', 'rgb_median_3_dxdy_3.png', 'gray_median_3_dxdy_3.png'],
                                 ['Median', '3', '5', 'rgb_median_3_dxdy_5.png', 'gray_median_3_dxdy_5.png'],
                                 ['Median', '5', '3', 'rgb_median_5_dxdy_3.png', 'gray_median_5_dxdy_3.png'],
                                 ['Median', '5', '5', 'rgb_median_5_dxdy_5.png', 'gray_median_5_dxdy_5.png'],
                                 ['Gaussian', '1', '3', 'rgb_gaussian_1_dxdy_3.png', 'gray_gaussian_1_dxdy_3.png'],
                                 ['Gaussian', '1', '5', 'rgb_gaussian_1_dxdy_5.png', 'gray_gaussian_1_dxdy_5.png'],
                                 ['Gaussian', '2', '3', 'rgb_gaussian_2_dxdy_3.png', 'gray_gaussian_2_dxdy_3.png'],
                                 ['Gaussian', '2', '5', 'rgb_gaussian_2_dxdy_5.png', 'gray_gaussian_2_dxdy_5.png'],
                                 ['Gaussian', '3', '3', 'rgb_gaussian_3_dxdy_3.png', 'gray_gaussian_3_dxdy_3.png'],
                                 ['Gaussian', '3', '5', 'rgb_gaussian_3_dxdy_5.png', 'gray_gaussian_3_dxdy_5.png'],
                                 ])

    # Write out all files to images directory. Write small versions to images/thumb.
    for name, image in images.items():
        if name != 'original':
            cv2.imwrite('{}/{}.png'.format(image_dir, name),image)
            cv2.imwrite('{}/{}/{}.png'.format(image_dir, 'thumb', name),
                            cv2.resize(image,None,fx=.1, fy=.1, interpolation = cv2.INTER_LINEAR))

    # Write out web page
    write_web_page(sections, links)


def write_web_page(sections, links):
    with open("index.html", "w") as web_page_file:
        write_header(web_page_file)
        for section in sections:
            write_table(web_page_file, section, *links[section])
        write_footer(web_page_file)

def write_header(web_page_file):
    web_page_file.write("""<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CS 682 &mdash; Assignment 1</title>

    <!-- Bootstrap -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

      <h1>CS 682 &mdash; Assignment 1. Paul McKerley</h1>
""")

def write_footer(web_page_file):
    web_page_file.write("""  </body>
</html>
""")

# Write out a table of data
def write_table(web_page_file, section, header, data):
    w = web_page_file.write
    w("""
<h2>{0}</h2>\n
<table class="table table-striped">
  <thead>
  <tr>
""".format(section))
    for h in header:
        w('<th>{0}</th>'.format(h))

    w("""
	</tr>
      </thead>
      <tbody>
""")
    for row in data:
        w("<tr>\n")
        for c in to_links(row):
            w("<td>{0}</td>\n".format(c))
        w("</tr>\n")
   
    w("""
      </tbody>
    </table>
""")

# If data is a file name (ends with .png) then make it an image link. Otherwise leave it unchanged.
def to_links(row):
    for d in row:
        if d.endswith('.png'):
            yield('<a href="images/{0}"><img src="images/thumb/{0}"></href>'.format(d))
        else:
            yield(str(d))
            
if __name__=='__main__':
    main(sys.argv[1])
    
